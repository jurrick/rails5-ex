FROM ruby:2.3.3

EXPOSE 8080
ENV RAILS_ENV production

RUN apt-get update -qq && apt-get install -y -qq nodejs npm

RUN mkdir -p /rails5
WORKDIR /rails5
ADD Gemfile /rails5/Gemfile
ADD Gemfile.lock /rails5/Gemfile.lock
RUN gem install bundler
RUN gem install rb-readline
RUN bundle install --jobs $(nproc)
ADD . /rails5
RUN chgrp -R 0 /rails5
RUN chmod -R g+rw /rails5
RUN find /rails5 -type d -exec chmod g+x {} +

CMD ["puma"]
